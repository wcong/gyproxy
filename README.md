### gyproxy
使用django框架，通过localhost地址，完成对高毅官方网站全部功能的访问

### 运行说明
目前只支持python2和django1.8+，推荐运行环境

* python2.7.10
* django1.8.7

### 运行步骤
1. 安装django, 已安装跳过:```pip install django==1.8.7```
2. 开启服务:```python manage.py runserver```
3. 访问 http://localhost:8000/