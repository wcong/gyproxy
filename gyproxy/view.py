# -*- coding: utf-8 -*-
import re

from django.http import HttpResponse
from django.utils.six.moves import urllib

REWRITE_REGEX = re.compile(r'((?:src|action|href)=["\'])/(?!\/)')

base_url = 'http://gyasset.com'

excluded_headers = {
    'connection', 'keep-alive', 'proxy-authenticate',
    'proxy-authorization', 'te', 'trailers', 'transfer-encoding',
    'upgrade',
    'content-encoding',
    'content-length',
}


def index(request):
    original_request_path = request.path
    regex = re.compile('^HTTP_')
    headers = dict(
        (regex.sub('', header), value) for (header, value) in request.META.items() if header.startswith('HTTP_'))
    request = normalize_request(request)
    response = get_response(request, headers)
    response = rewrite_response(original_request_path, request, response)
    return response


def normalize_request(request):
    """
    处理path
    """
    if not request.path.startswith('/'):
        request.path = '/' + request.path
    request.path = request.path
    request.path_info = request.path
    request.META['PATH_INFO'] = request.path
    return request


def rewrite_response(original_request_path, request, response):
    """
    替换高毅域名的硬编码
    """
    proxy_root = original_request_path.rsplit(request.path, 1)[0]
    response.content = REWRITE_REGEX.sub(r'\1{}/'.format(proxy_root), response.content)
    return response


def create_request(url, body=None, headers={}):
    """
    生成urllib request, request内部是通过body is None 判断 http method, 特殊处理下body
    """
    del headers['HOST']
    if body is not None and len(body) == 0:
        body = None
    request = urllib.request.Request(url, body, headers)
    return request


def get_response(request, headers):
    """
    代理请求高毅的网站，并返回django的response,并设置header
    """
    request_url = get_full_url(request)
    request = create_request(request_url, request.body, headers)
    response = urllib.request.urlopen(request)
    try:
        response_body = response.read()
        status = response.getcode()
    except urllib.error.HTTPError as e:
        response_body = e.read()
        status = e.code
    proxy_response = HttpResponse(response_body, status=status, content_type=response.headers['content-type'])
    for header_str in response.headers.headers:
        key, value = header_str.split(':', 1)
        if key.lower() in excluded_headers:
            continue
        proxy_response[key] = value.strip()
    return proxy_response


def get_full_url(request):
    """
    拼接域名和GET参数，返回完整的url
    """
    param_str = request.GET.urlencode()
    request_url = u'%s%s' % (base_url, request.path)
    request_url += '?%s' % param_str if param_str else ''
    return request_url
